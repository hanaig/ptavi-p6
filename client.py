#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys

# Cliente UDP simple.

# Dirección IP del servidor.
try:
    info = sys.argv[2]
    ip = info.split(':')[-2]
    ipBn = ip.split('@')[-1]
    puerto = int(info.split(':')[-1])
    PORT = puerto
    SERVER = ipBn
    metodo = sys.argv[1]

    if metodo != "INVITE" and metodo != "ACK" and metodo != "BYE":
        raise ValueError
    if metodo == 'INVITE':
        line = ('INVITE' + ' sip:' + ip + ' SIP/2.0\r\n')
        line = line + ('Content-Lenght:' + str(len('v=0\r\n' + '0='
                                                   + 'usuario@gotham.com '
                                                   + ipBn + '\r\n' + 's='
                                                   + 'miSesion' + '\r\n'
                                                   + 't=' + '0' + '\r\n'
                                                   + 'm=' + 'audio '
                                                   + str(puerto * 2)
                                                   + ' ' + 'RTP'
                                                   + '\r\n\r\n')) + '\r\n\r\n')
        line = line + 'v=0\r\n'
        line = line + ('0=' + 'usuario@gotham.com ' + ipBn + '\r\n')
        line = line + ('s=' + 'miSesion' + '\r\n')
        line = line + ('t=' + '0' + '\r\n')
        line = line + ('m=' + 'audio ' + str(puerto * 2) + ' ' + 'RTP')
    elif metodo == 'BYE':
        line = ('BYE' + ' sip:' + ip + ' SIP/2.0\r\n')
    else:
        line = metodo

    # Contenido que vamos a enviar

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((SERVER, PORT))

        print("Enviando: " + line)
        my_socket.send(bytes(line, 'utf-8') + b'\r\n')
        data = my_socket.recv(1024)
        respuestaServidor = data.decode('utf-8')
        print('Recibido -- ', respuestaServidor)
        if metodo == 'INVITE':
            respuestaServidor = respuestaServidor.split('\r\n\r\n')
            respuestaServidor = respuestaServidor[2]
            if respuestaServidor == 'SIP/2.0 200 OK':
                line = ('ACK' + ' sip:' + ip + ' SIP/2.0')
                my_socket.send(bytes(line, 'utf-8') + b'\r\n')
        print("Terminando socket...")

    print("Fin.")

except (IndexError, ValueError):
    sys.exit('Usage: python3 client.py method receiver@IP:SIPport')
