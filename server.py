#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import simplertp
import random

datos = []


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        global datos

        line = self.rfile.read()
        mensajeCliente = line.decode('utf-8')
        if mensajeCliente != '\r\n':
            print("El cliente nos manda ", mensajeCliente)
            datosCliente = mensajeCliente.split()
            metodo = datosCliente[0]
            peticion = datosCliente[1].split(':')[0]
            version = datosCliente[2]
            if peticion != 'sip' or version != 'SIP/2.0':
                respuesta = b'Bad Request\r\n\r\n'
                self.wfile.write(respuesta)
            else:
                if metodo == 'INVITE':
                    respuesta = \
                        b'SIP/2.0 100 ' \
                        b'Trying\r\n\r\nSIP/2.0 180 ' \
                        b'Ringing\r\n\r\nSIP/2.0 ' \
                        b'200 OK\r\n\r\n'
                    self.wfile.write(respuesta)
                    datos = datosCliente
                elif metodo == 'BYE':
                    respuesta = b'SIP/2.0 200 OK\r\n\r\n'
                    self.wfile.write(respuesta)
                elif metodo == 'ACK':
                    ALEAT = int(random.random()
                                * 100000)
                    ip = datos[6]
                    port = int(datos[-2])
                    RTP_header = simplertp.RtpHeader()
                    RTP_header.set_header(pad_flag=0,
                                          ext_flag=0,
                                          cc=0, marker=0,
                                          ssrc=ALEAT)
                    audio = simplertp.RtpPayloadMp3(audio_file)
                    simplertp.send_rtp_packet(RTP_header, audio, ip, port)
                else:
                    respuesta = b'Method Not Allowed\r\n\r\n'
                    self.wfile.write(respuesta)


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    try:
        puerto1 = int(sys.argv[2])
        ip1 = sys.argv[1]
        audio_file = sys.argv[3]
        serv = socketserver.UDPServer((ip1, puerto1), EchoHandler)

        print("Listening...")
        try:
            serv.serve_forever()
        except KeyboardInterrupt:
            print("Finalizado")
    except (IndexError, ValueError):
        sys.exit('Usage: python3 server.py IP port audio_file')
